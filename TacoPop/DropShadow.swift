//
//  DropShadow.swift
//  TacoPop
//
//  Created by Tim Chen on 3/28/17.
//  Copyright © 2017 Tim Chen. All rights reserved.
//

import UIKit

protocol DropShadow {
    
}

extension DropShadow where Self: UIView {
    func addDropShadow() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.7
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 5
    }
}
