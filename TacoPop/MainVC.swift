//
//  MainVC.swift
//  TacoPop
//
//  Created by Tim Chen on 3/28/17.
//  Copyright © 2017 Tim Chen. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    @IBOutlet weak var headerView: HeaderView!

    override func viewDidLoad() {
        super.viewDidLoad()

        headerView.addDropShadow()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
